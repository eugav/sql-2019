create user eugene identified by eugene;
grant connect, resource to eugene;
grant create any view to eugene;

grant select on oltp_test.city to public;
grant select on oltp_test.product to public;
grant select on oltp_test.office to public;
grant select on oltp_test.sales_order to public;
grant select on oltp_test.sales_order_line to public;
grant select on oltp_test.v_fact_sale to public;

-- connect eugene/eugene@orcl
create table temp_fact_sale as select * from oltp_test.v_fact_sale;
