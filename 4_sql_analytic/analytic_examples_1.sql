with amount_per_month as (select TO_CHAR(s.ORDER_DATE, 'MM') MONTH,
                                 SUM(PRODUCT_PRICE * PRODUCT_QTY) AMOUNT
                          from SALES_ORDER_LINE sol
                                 inner join SALES_ORDER S on sol.SALES_ORDER_ID = S.SALES_ORDER_ID
                          where TO_CHAR(s.ORDER_DATE, 'YYYY') = 2016
                          group by TO_CHAR(s.ORDER_DATE, 'MM')
                          order by TO_CHAR(s.ORDER_DATE, 'MM'))
     select a1.MONTH, a1.AMOUNT, sum(a2.AMOUNT)
from amount_per_month a1
  inner join amount_per_month a2 on (a2.MONTH <= a1.MONTH)
group by a1.MONTH, a1.AMOUNT
order by month;

with amount_per_month as (select TO_CHAR(s.ORDER_DATE, 'MM') MONTH,
                                 SUM(PRODUCT_PRICE * PRODUCT_QTY) AMOUNT
                          from SALES_ORDER_LINE sol
                                 inner join SALES_ORDER S on sol.SALES_ORDER_ID = S.SALES_ORDER_ID
                          where TO_CHAR(s.ORDER_DATE, 'YYYY') = 2016
                          group by TO_CHAR(s.ORDER_DATE, 'MM')
                          order by TO_CHAR(s.ORDER_DATE, 'MM'))
select MONTH,
       AMOUNT,
       SUM(AMOUNT) over (
         order by MONTH rows between unbounded preceding and current row
         ) CUM_SUM,
       SUM(AMOUNT) over (
         order by TO_NUMBER(MONTH) range between TO_NUMBER(MONTH)-1 PRECEDING and current row
         ) CUM_SUM2,
       LAG(AMOUNT, 1, 0) over (order by MONTH) PREVIOUS_AMOUNT,
       (AMOUNT - LAG(AMOUNT, 1, 0)  over (order by MONTH)) / LAG(AMOUNT, 1, AMOUNT)  over (order by MONTH) * 100
from amount_per_month;


select MANAGER_ID,
       MANAGER_FIRST_NAME,
       MANAGER_LAST_NAME,
       AMOUNT,
       YEAR,
      FIRST_VALUE(AMOUNT) over (partition by YEAR order by AMOUNT DESC) BEST_SALE,
      CUME_DIST() over (partition by YEAR order by AMOUNT),
      PERCENTILE_CONT(0.05) WITHIN GROUP (order by AMOUNT)
          over (partition by YEAR) "5%",
       PERCENTILE_CONT(0.25) WITHIN GROUP (order by AMOUNT)
           over (partition by YEAR) "25%",
       PERCENTILE_CONT(0.50) WITHIN GROUP (order by AMOUNT)
           over (partition by YEAR) "50%",
       PERCENTILE_CONT(0.75) WITHIN GROUP (order by AMOUNT)
           over (partition by YEAR) "75%",
       PERCENTILE_CONT(0.95) WITHIN GROUP (order by AMOUNT)
           over (partition by YEAR) "95%"
       -- DENSE_RANK() over (partition by YEAR order by AMOUNT DESC),
       -- RANK() over (order by AMOUNT DESC),
       -- (AMOUNT - AVG(AMOUNT) over (partition by YEAR, MANAGER_FIRST_NAME)) /
       --     AVG(AMOUNT) over (partition by YEAR, MANAGER_FIRST_NAME) * 100
       from (select MANAGER_ID,
                    MANAGER_FIRST_NAME,
                    MANAGER_LAST_NAME,
                    sum(SALE_AMOUNT) AMOUNT,
                    TO_CHAR(SALE_DATE, 'YYYY') YEAR
             from V_FACT_SALE
             group by MANAGER_ID,
                      MANAGER_FIRST_NAME,
                      MANAGER_LAST_NAME,
                      TO_CHAR(SALE_DATE, 'YYYY')
)