with AMOUNT_PER_MONTH as (select TO_CHAR(SO.ORDER_DATE, 'MM')             MONTH,
                                 sum(SOL.PRODUCT_PRICE * SOL.PRODUCT_QTY) AMOUNT
                          from SALES_ORDER SO
                                 left outer join SALES_ORDER_LINE SOL
                                   on (SO.SALES_ORDER_ID = SOL.SALES_ORDER_ID)
                          where TO_CHAR(SO.ORDER_DATE, 'YYYY') = 2016
                          group by TO_CHAR(SO.ORDER_DATE, 'MM')
                          order by TO_CHAR(SO.ORDER_DATE, 'MM'))

select A1.MONTH, A1.AMOUNT, SUM(A2.AMOUNT) from AMOUNT_PER_MONTH A1
  inner join AMOUNT_PER_MONTH A2 on (A2.MONTH <= A1.MONTH)
  group by A1.MONTH, A1.AMOUNT
  order by A1.MONTH;

with AMOUNT_PER_MONTH as (select TO_CHAR(SO.ORDER_DATE, 'MM')             MONTH,
                                 sum(SOL.PRODUCT_PRICE * SOL.PRODUCT_QTY) AMOUNT
                          from SALES_ORDER SO
                                 left outer join SALES_ORDER_LINE SOL
                                   on (SO.SALES_ORDER_ID = SOL.SALES_ORDER_ID)
                          where TO_CHAR(SO.ORDER_DATE, 'YYYY') = 2016
                          group by TO_CHAR(SO.ORDER_DATE, 'MM')
                          order by TO_CHAR(SO.ORDER_DATE, 'MM'))

select MONTH,
       AMOUNT,
       SUM(AMOUNT) over (order by MONTH),
       SUM(AMOUNT) over (order by MONTH DESC)
from AMOUNT_PER_MONTH
order by MONTH;


with AMOUNT_PER_MONTH as (select TO_CHAR(SO.ORDER_DATE, 'MM')             MONTH,
                                 SO.MANAGER_ID,
                                 sum(SOL.PRODUCT_PRICE * SOL.PRODUCT_QTY) AMOUNT
                          from SALES_ORDER SO
                                 left outer join SALES_ORDER_LINE SOL
                                   on (SO.SALES_ORDER_ID = SOL.SALES_ORDER_ID)
                          where TO_CHAR(SO.ORDER_DATE, 'YYYY') = 2016
                          group by TO_CHAR(SO.ORDER_DATE, 'MM'), SO.MANAGER_ID
                          order by TO_CHAR(SO.ORDER_DATE, 'MM'))
select MONTH,
       AMOUNT,
       MANAGER_ID,
       (AMOUNT - AVG(AMOUNT) over (partition by MONTH)) / AVG(AMOUNT) over (partition by MONTH) * 100 DEV,
       avg(AMOUNT) over (partition by month order by amount rows between UNBOUNDED PRECEDING and CURRENT ROW) AVG_LESS,
       LAST_VALUE(MANAGER_ID) over (partition by month order by AMOUNT ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING),
from AMOUNT_PER_MONTH
order by MONTH;


select MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME,
       MONTH,
       AMOUNT,
       DENSE_RANK() over (partition by MONTH order by AMOUNT DESC),
       RANK() over (partition by MONTH order by AMOUNT DESC),
       ROW_NUMBER()  over (partition by MONTH order by AMOUNT DESC)
from (select MANAGER_ID,
                      MANAGER_LAST_NAME,
                      MANAGER_FIRST_NAME,
                      TO_CHAR(SALE_DATE, 'MM') MONTH,
                      SUM(SALE_AMOUNT) AMOUNT
               from V_FACT_SALE
               where TO_CHAR(SALE_DATE, 'YYYY') = 2016
               group by MANAGER_ID, MANAGER_LAST_NAME, MANAGER_FIRST_NAME, TO_CHAR(SALE_DATE, 'MM'))

select OFFICE_ID,
    LISTAGG(MANAGER_LAST_NAME || ' ' || MANAGER_FIRST_NAME, ', ') WITHIN GROUP (order by MANAGER_ID)
    from MANAGER
    group by OFFICE_ID;
