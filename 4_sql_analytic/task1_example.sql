-- ============ Вариант 1 (не очень правильный) ===================

-- Каждый месяц компания выдает премию в размере 5%
-- от суммы продаж менеджеру, который за предыдущие 3 месяца
-- продал товаров на самую большую сумму
-- Выведите месяц, manager_id, manager_first_name, manager_last_name, премию за период с января по декабрь 2014 года

-- Шаг 1.
-- В задании требуется обрабатывать временные интервалы. это можно сделать с помощью
--функции sql months_between (date1,date2)
-- если date1 > date2, тo результат положительный, иначе - отрицательный

select months_between('06-Jun-2013','01-jan-2013') from dual;

-- В результате получается вещественное число 5.16129032, которое нас не устраивает
-- усечем его до целого с помощью sql функции trunc

select trunc(months_between('06-jun-2013','01-jan-2013')) month from dual;

-- В результате получим целое число месяцев - 5 , назовем его month

-- Применим эту реализацию к условию задания

--Подсчитаем количество месяцев + 1(так как возможная премия будет в следующем месяце)
--по всем продажам, которое будет участвовать в
--отборе 3-х последних в заданный условием задания
--временной период: 10,11,12 месяцы 2013 года, 1-12 месяцы 2014

select distinct sale_date, TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH
from mv_fact_sale
where sale_date between TO_DATE('01-oct-2013', 'DD-MON-YYYY')
                    and TO_DATE('31-dec-2014', 'DD-MON-YYYY');

--выбрано 4190 строк

--добавим в выборку требуемые в задании атрибуты
select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
       manager_id,
       manager_first_name,
       manager_last_name
from mv_fact_sale where sale_date between '01-oct-2013' and '31-dec-2014';

--теперь добавим суммирование по продажам и группировку по комплексу

select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
       manager_id,
       manager_first_name,
       manager_last_name,
       sum(sale_amount) SALE_AMOUNT
from mv_fact_sale
where sale_date between '01-oct-2013' and '31-dec-2014'
    --
group by (
    TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
    manager_id,
    manager_first_name,
    manager_last_name );
-- having TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 = 3;
-- having MANAGER_FIRST_NAME = 'Eugene';

-- именуем выборку STEP1

( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
         manager_id,
         manager_first_name,
         manager_last_name,
         sum(sale_amount) SALE_AMOUNT
  from mv_fact_sale
  where sale_date between '01-oct-2013' and '31-dec-2014'
    --
  group by (
      TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
      manager_id,
      manager_first_name,
      manager_last_name )) STEP1;

-- и шаг 1 завершен выбрана 751 запись

--Шаг 2.
-- теперь обернем STEP1, группировкой продаж по равенству manager_id за 3 месяца,
-- начиная с даты продажи

select MONTH, manager_id, manager_first_name, manager_last_name,
       SALE_AMOUNT,
       SUM(sale_amount) OVER (PARTITION BY manager_id order by MONTH RANGE between 3 PRECEDING and 1 preceding) PERIOD_SALE_AMOUNT
from
     ( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
              manager_id,
              manager_first_name,
              manager_last_name,
              sum(sale_amount) SALE_AMOUNT
       from mv_fact_sale
       where sale_date between '01-oct-2013' and '31-dec-2014'
         --
       group by (
           TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
           manager_id,
           manager_first_name,
           manager_last_name )) STEP1
-- where manager_id = 969
order by manager_id, month;

-- именуем выборку STEP2

(select MONTH, manager_id, manager_first_name, manager_last_name,
        SUM(sale_amount) OVER (PARTITION BY manager_id order by MONTH RANGE between 3 PRECEDING and 1 preceding) PERIOD_SALE_AMOUNT
 from
      ( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
               manager_id,
               manager_first_name,
               manager_last_name,
               sum(sale_amount) SALE_AMOUNT
        from mv_fact_sale
        where sale_date between '01-oct-2013' and '31-dec-2014'
          --
        group by (
            TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
            manager_id,
            manager_first_name,
            manager_last_name )) STEP1) STEP2;

-- шаг 2 завершен выбрана 751 запись
--
-- Шаг 3
-- Обернем STEP2 запросом поиска MAX, группировкой по равенству периода
--
select MONTH, manager_id, manager_first_name, manager_last_name, PERIOD_SALE_AMOUNT,
       MAX(PERIOD_SALE_AMOUNT) over (partition by MONTH) MAX_MONTH_SALE_AMOUNT
from
     (select MONTH, manager_id, manager_first_name, manager_last_name,
             SUM(sale_amount) OVER (PARTITION BY manager_id order by MONTH RANGE between 3 PRECEDING and 1 preceding) PERIOD_SALE_AMOUNT
      from
           ( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
                    manager_id,
                    manager_first_name,
                    manager_last_name,
                    sum(sale_amount) SALE_AMOUNT
             from mv_fact_sale
             where sale_date between '01-oct-2013' and '31-dec-2014'
               --
             group by (
                 TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
                 manager_id,
                 manager_first_name,
                 manager_last_name )) STEP1) STEP2;

-- именуем  выборку STEP3

(select MONTH, manager_id, manager_first_name, manager_last_name, PERIOD_SALE_AMOUNT,
        MAX(PERIOD_SALE_AMOUNT) over (partition by MONTH) MAX_MONTH_SALE_AMOUNT
 from
      (select MONTH, manager_id, manager_first_name, manager_last_name,
              SUM(sale_amount) OVER (PARTITION BY manager_id order by MONTH RANGE between -3 PRECEDING  and 1 preceding) PERIOD_SALE_AMOUNT
       from
            ( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
                     manager_id,
                     manager_first_name,
                     manager_last_name,
                     sum(sale_amount) SALE_AMOUNT
              from mv_fact_sale
              where sale_date between '01-oct-2013' and '31-dec-2014'
                --
              group by (
                  TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
                  manager_id,
                  manager_first_name,
                  manager_last_name )) STEP1) STEP2) STEP3;

--шаг 3 завершен, выбрана 751 запись

--Заключительный шаг
--обернем STEP3 запросом, в которм вычислим вознаграждение, того менеджера, который продал
--товаров на максимальную сумму за предыдущие 3 месяца

select MONTH, manager_id, manager_first_name, manager_last_name, PERIOD_SALE_AMOUNT, 0.05 * PERIOD_SALE_AMOUNT BONUS_AMOUNT
from
     (select MONTH, manager_id, manager_first_name, manager_last_name, PERIOD_SALE_AMOUNT,
             MAX(PERIOD_SALE_AMOUNT) over (partition by MONTH) MAX_MONTH_SALE_AMOUNT
      from
           (select MONTH, manager_id, manager_first_name, manager_last_name,
                   SUM(sale_amount) OVER (PARTITION BY manager_id order by MONTH RANGE between 3 PRECEDING and 1 preceding) PERIOD_SALE_AMOUNT
            from
                 ( select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
                          manager_id,
                          manager_first_name,
                          manager_last_name,
                          sum(sale_amount) SALE_AMOUNT
                   from mv_fact_sale
                   where sale_date between '01-oct-2013' and '31-dec-2014'
                     --
                   group by (
                       TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
                       manager_id,
                       manager_first_name,
                       manager_last_name )) STEP1) STEP2) STEP3
    --
where PERIOD_SALE_AMOUNT = MAX_MONTH_SALE_AMOUNT and MONTH > 0;

-- результат
MONTH	MANAGER_ID	MANAGER_FIRST_NAME	MANAGER_LAST_NAME	PERIOD_SALE_AMOUNT	BONUS_AMOUNT
1	362	Eugene	Miller	133728.93	6686.4465
2	969	Gregory	Cox	140929.22	7046.461
3	813	Nancy	Carter	140901.5	7045.075
4	214	Lawrence	Gordon	171632.29	8581.6145
5	158	Kenneth	Lynch	138905.34	6945.267
6	1	Walter	Ford	171148.43	8557.4215
7	366	Michael	Evans	163647.85	8182.3925
8	1	Walter	Ford	193947.72	9697.386
9	66	Jesse	Graham	235568.49	11778.4245
10	643	Theresa	Stone	127531.92	6376.596

SALE_MONTH	MANAGER_ID	MANAGER_FIRST_NAME	MANAGER_LAST_NAME	BONUS	MAX_BONUS
2014-01-01	362	Eugene	Miller	10610.4775	10610.4775
2014-02-01	362	Eugene	Miller	13942.096	13942.096
2014-03-01	969	Gregory	Cox	15049.106	15049.106
2014-04-01	191	Clarence	Woods	14948.0415	14948.0415
2014-05-01	191	Clarence	Woods	14948.0415	14948.0415
2014-06-01	191	Clarence	Woods	14948.0415	14948.0415
2014-07-01	1	Walter	Ford	12245.8705	12245.8705
2014-08-01	366	Michael	Evans	11110.552	11110.552
2014-09-01	66	Jesse	Graham	11778.4245	11778.4245
2014-10-01	958	Sharon	Jackson	10160.778	10160.778

select MANAGER_ID, MANAGER_LAST_NAME, MANAGER_FIRST_NAME, SUM(SALE_AMOUNT) from OLTP_TEST.MV_FACT_SALE
    where SALE_DATE between TO_DATE('01.12.2013', 'DD.MM.YYYY') and TO_DATE('28.02.2014', 'DD.MM.YYYY')
    group by MANAGER_ID, MANAGER_LAST_NAME, MANAGER_FIRST_NAME
    order by SUM(SALE_AMOUNT) desc;


select level from dual connect by level <=15;

select ADD_MONTHS(TO_DATE('01.10.2013', 'DD.MM.YYYY'), level-1) SALE_MONTH
from dual connect by level <= 15;

select manager_id, MANAGER_FIRST_NAME, MANAGER_LAST_NAME from MANAGER;



select
    MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME,
    sum(SALE_AMOUNT)
from MV_FACT_SALE
where
    sale_date between TO_DATE('01-10-2013', 'DD-MM-YYYY') and TO_DATE('31-12-2014', 'DD-MM-YYYY')
    and TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 between -1 and 1
group by manager_id, MANAGER_FIRST_NAME, MANAGER_LAST_NAME;


-- запрос может быть также реализован с помощью WITH
WITH
STEP1 as (select TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1 MONTH,
manager_id,
manager_first_name,
manager_last_name,
sum(sale_amount) SALE_AMOUNT
from MV_FACT_SALE
where sale_date between '01-oct-2013' and '31-dec-2014'
group by TRUNC(MONTHS_BETWEEN(sale_date, '01-jan-2014')) + 1,
manager_id,
manager_first_name,
manager_last_name),
--
STEP2 as (select MONTH, manager_id, manager_first_name, manager_last_name,
--
SUM(SALE_AMOUNT) OVER (PARTITION BY manager_id order by month range 3 preceding) PERIOD_SALE_AMOUNT
from STEP1),
--
STEP3 as (select MONTH, manager_id, manager_first_name, manager_last_name, PERIOD_SALE_AMOUNT,
--
MAX (PERIOD_SALE_AMOUNT) OVER (PARTITION BY MONTH) MAX_MONTH_SALE_AMOUNT
from STEP2)
--
select MONTH,
       manager_id,
       substr(manager_first_name, 1, 10),
       substr(manager_last_name, 1, 10),
       0.05 * PERIOD_SALE_AMOUNT BONUS_AMOUNT
from STEP3
where PERIOD_SALE_AMOUNT = MAX_MONTH_SALE_AMOUNT
  and MONTH > 0;





with TV_MANAGER_SALES as (select TRUNC(MONTHS_BETWEEN(SALE_DATE, TO_DATE('01.01.2014', 'DD.MM.YYYY'))) + 1 MONTH,
                                 manager_id,
                                 MANAGER_FIRST_NAME,
                                 MANAGER_LAST_NAME,
                                 sum(SALE_AMOUNT) SALE_AMOUNT
                          from MV_FACT_SALE
                          where SALE_DATE between
                                    TO_DATE('01.10.2013', 'DD.MM.YYYY') and
                                    TO_DATE('31.12.2014', 'DD.MM.YYYY')
                          group by TRUNC(MONTHS_BETWEEN(SALE_DATE, TO_DATE('01.01.2014', 'DD.MM.YYYY'))) + 1,
                                   manager_id,
                                   MANAGER_FIRST_NAME,
                                   MANAGER_LAST_NAME
    ),
     TV_MANAGER_3MONTHS_SALES as (
                          select MONTH, MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME,
                                 SUM(SALE_AMOUNT) over (partition by MANAGER_ID order by month range 3 preceding) PERIOD_SALE_AMOUNT
                          from TV_MANAGER_SALES
    ),
     TV_MAX_SALE_AMOUNT as (
                          select MONTH, MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME, PERIOD_SALE_AMOUNT,
                                 MAX(PERIOD_SALE_AMOUNT) over (partition by MONTH) MAX_MONTH_SALE_AMOUNT
                          from TV_MANAGER_3MONTHS_SALES
    )
select MONTH, MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME, 0.05 * PERIOD_SALE_AMOUNT BONUS_AMOUNT
from TV_MAX_SALE_AMOUNT
where PERIOD_SALE_AMOUNT = MAX_MONTH_SALE_AMOUNT and MONTH > 0;


-- ============ Вариант 2 (правильный) ===================

with ALL_MONTHS as (
    select ADD_MONTHS(TO_DATE('01.10.2013', 'DD.MM.YYYY'), level-1) SALE_MONTH
        from dual connect by level <= 15
),
     ALL_MANAGERS as (
         select MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME from MANAGER
     ),
     ALL_MANAGERS_MONTHS as (
         select SALE_MONTH, MANAGER_ID, MANAGER_FIRST_NAME, MANAGER_LAST_NAME
            from ALL_MONTHS cross join ALL_MANAGERS
     ),
     step1 as
         (
             select
                 SALES_ORDER_ID,
                 SALE_MONTH,
                 M.manager_id,
                 M.MANAGER_FIRST_NAME,
                 M.MANAGER_LAST_NAME,
                 SALE_AMOUNT
             from MV_FACT_SALE F
                      right outer join ALL_MANAGERS_MONTHS M on (TRUNC(SALE_DATE, 'MONTH')=M.SALE_MONTH and F.MANAGER_ID=M.MANAGER_ID)
         ),
--         select * from step1 where manager_id=362;
     step2 as (select SALES_ORDER_ID,
                      SALE_MONTH,
                      manager_id,
                      MANAGER_FIRST_NAME,
                      MANAGER_LAST_NAME,
                      SALE_AMOUNT,
                      sum(SALE_AMOUNT) over (partition by MANAGER_ID order by SALE_MONTH RANGE BETWEEN INTERVAL '3' MONTH PRECEDING AND INTERVAL '1' MONTH PRECEDING) PREV_SALE_AMOUNT
               from step1 F
               where SALE_MONTH between
                         TO_DATE('01.10.2013', 'DD.MM.YYYY') and
                         TO_DATE('31.12.2014', 'DD.MM.YYYY')
--   and MANAGER_ID = 362
     ),
     step3 as (
         select SALE_MONTH,
                manager_id,
                MANAGER_FIRST_NAME,
                MANAGER_LAST_NAME,
                MAX(PREV_SALE_AMOUNT)*0.05 BONUS
         from STEP2
         group by
             SALE_MONTH,
             manager_id,
             MANAGER_FIRST_NAME,
             MANAGER_LAST_NAME
     ),
     step4 as (
         select
             SALE_MONTH,
             manager_id,
             MANAGER_FIRST_NAME,
             MANAGER_LAST_NAME,
             BONUS,
             MAX(BONUS) OVER (PARTITION BY SALE_MONTH) MAX_BONUS
         from STEP3
     )
select * from STEP4 where MAX_BONUS = BONUS and SALE_MONTH >= TO_DATE('01.01.2014', 'DD.MM.YYYY');

analyze table MANAGER compute statistics for columns MANAGER_FIRST_NAME;

select * from USER_TAB_COL_STATISTICS where table_name = 'MANAGER';
select * from USER_TAB_HISTOGRAMS where table_name = 'MANAGER' and column_name='MANAGER_FIRST_NAME';