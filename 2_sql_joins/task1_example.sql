select * from city where CITY_ID = 391;

select * from OFFICE where OFFICE_ID = 485;

select count(*) from manager;

select M.MANAGER_FIRST_NAME, M.MANAGER_LAST_NAME,
       O.OFFICE_NAME, C.CITY_NAME, C.REGION, C.COUNTRY
  from manager M
  inner join OFFICE O on (M.OFFICE_ID = O.OFFICE_ID)
  inner join CITY C on (C.CITY_ID = O.CITY_ID);


select M.MANAGER_FIRST_NAME, M.MANAGER_LAST_NAME,
       O.OFFICE_NAME, C.CITY_NAME, C.REGION, C.COUNTRY
from (select * from MANAGER MM
          where exists (
              select * from SALES_ORDER SO where SO.MANAGER_ID = MM.MANAGER_ID
                    )
     ) M
       inner join OFFICE O on (M.OFFICE_ID = O.OFFICE_ID)
       inner join CITY C on (C.CITY_ID = O.CITY_ID)

select M.MANAGER_FIRST_NAME,
       M.MANAGER_LAST_NAME,
       (select OFFICE_NAME from OFFICE O where O.OFFICE_ID = M.OFFICE_ID and ROWNUM <= 1),
       (select C.CITY_NAME from CITY C where C.CITY_ID =
            (select O.CITY_ID from OFFICE O where O.OFFICE_ID = M.OFFICE_ID))
--       C.CITY_NAME,
--       C.REGION,
--       C.COUNTRY
from manager M
--       inner join OFFICE O on (M.OFFICE_ID = O.OFFICE_ID)
--       inner join CITY C on (C.CITY_ID = O.CITY_ID)

select MM.* from (
  select M.*, ROWNUM RN from (
        select * from MANAGER
    ) M where ROWNUM < 30
) MM where RN >= 20;


-- Вывести имена и фамилии менеджер(ов),
-- продавшего товаров в январе 2016 года
-- на наибольшую сумму.

select MANAGER_ID, SUM(PRODUCT_QTY*PRODUCT_PRICE) from SALES_ORDER_LINE SOL
    inner join SALES_ORDER S on SOL.SALES_ORDER_ID = S.SALES_ORDER_ID
    where S.ORDER_DATE BETWEEN TO_DATE('01.01.2016', 'DD.MM.YYYY') and
              TO_DATE('31.01.2016', 'DD.MM.YYYY')
    group by MANAGER_ID;

-- Common Table Expressions

with MANAGER_SALES as (
    select MANAGER_ID, SUM(PRODUCT_QTY * PRODUCT_PRICE) AMOUNT
    from SALES_ORDER_LINE SOL
           inner join SALES_ORDER S on SOL.SALES_ORDER_ID = S.SALES_ORDER_ID
    where S.ORDER_DATE BETWEEN TO_DATE('01.01.2016', 'DD.MM.YYYY') and
              TO_DATE('31.01.2016', 'DD.MM.YYYY')
    group by MANAGER_ID
)
select * from MANAGER where MANAGER_ID in (
        select S.MANAGER_ID
         from MANAGER_SALES S
         where AMOUNT = (select MAX(AMOUNT) from MANAGER_SALES)
);

select CITY_NAME, 'CITY' from CITY
union
select REGION, 'REGION' from CITY
union
select COUNTRY, 'COUNTRY' from CITY;

with managers as (select manager_first_name,
                         manager_last_name,
                         sum(product_price * product_qty) price
                  from manager man
                         join sales_order sale on man.manager_id = sale.manager_id
                         join sales_order_line orderline
                           on sale.sales_order_id = orderline.sales_order_id
                  where order_date >= TO_DATE('01/01/2016', 'DD/MM/YYYY')
                    AND order_date <= TO_DATE('31/01/2016', 'DD/MM/YYYY')
                  group by manager_first_name, manager_last_name)
select manager_first_name, manager_last_name
from (select max(price) price from managers) maximum,
     managers
where managers.price = maximum.price;

SELECT MANAGER_FIRST_NAME, MANAGER_LAST_NAME
FROM MANAGER
WHERE MANAGER_ID IN (
                    SELECT MANAGER_ID
                    FROM (
                         SELECT MANAGER_ID, SUM(PRODUCT_PRICE * PRODUCT_QTY) TOTAL
                         FROM
                              SALES_ORDER_LINE SOL
                                INNER JOIN SALES_ORDER SO
                                  ON SOL.SALES_ORDER_ID = SO.SALES_ORDER_ID
                         WHERE order_date BETWEEN TO_DATE('1.01.2016', 'DD.MM.YYYY') AND TO_DATE('31.01.2016', 'DD.MM.YYYY')
                         GROUP BY MANAGER_ID
                         )
                    WHERE TOTAL = (SELECT MAX(t)
                                   FROM
                                        (SELECT MANAGER_ID, SUM(PRODUCT_PRICE * PRODUCT_QTY) t
                                         FROM
                                              SALES_ORDER_LINE SOL
                                                INNER JOIN SALES_ORDER SO
                                                  ON SOL.SALES_ORDER_ID = SO.SALES_ORDER_ID
                                         WHERE order_date BETWEEN TO_DATE('1.01.2016', 'DD.MM.YYYY') AND TO_DATE('31.01.2016', 'DD.MM.YYYY')
                                         GROUP BY MANAGER_ID
                                        )
                                  )
                    )

select COUNTRY from  CITY
UNION
select CITY_NAME from CITY
UNION
select REGION from CITY;

select MANAGER.MANAGER_FIRST_NAME, MANAGER.MANAGER_LAST_NAME from MANAGER inner join
  (select sum(SOL.PRODUCT_QTY * SOL.PRODUCT_PRICE) SALES_VOLUME, SO.MANAGER_ID from SALES_ORDER SO
                                                                                      inner join
                                                                                        SALES_ORDER_LINE SOL on (SO.SALES_ORDER_ID = SOL.SALES_ORDER_ID) where
       SO.ORDER_DATE >= TO_DATE('01.01.2016', 'DD.MM.YYYY') and ORDER_DATE <= TO_DATE('30.01.2016', 'DD.MM.YYYY')
   group by SO.MANAGER_ID order by SALES_VOLUME DESC) SALES_DATA
  on (MANAGER.MANAGER_ID = SALES_DATA.MANAGER_ID) where ROWNUM = 1;



SELECT managers.manager_first_name, managers.manager_last_name, summa
FROM (SELECT MAX(summa) maximum
      FROM((SELECT manager_first_name, manager_last_name, SUM(product_qty * product_price) summa
            FROM manager mg JOIN sales_order sale ON mg.manager_id = sale.manager_id
                            JOIN sales_order_line orderln ON sale.sales_order_id = orderln.sales_order_id
            WHERE order_date BETWEEN to_date('01-01-2016','DD-MM-YYYY') AND to_date('30-01-2016','DD-MM-YYYY')
            GROUP BY manager_first_name, manager_last_name))) max_price ,
     (SELECT manager_first_name, manager_last_name, SUM(product_qty * product_price) summa
      FROM manager mg JOIN sales_order sale ON mg.manager_id = sale.manager_id
                      JOIN sales_order_line orderln ON sale.sales_order_id = orderln.sales_order_id
      WHERE order_date BETWEEN to_date('01-01-2016','DD-MM-YYYY') AND to_date('30-01-2016','DD-MM-YYYY')
      GROUP BY manager_first_name, manager_last_name)managers
WHERE managers.summa = max_price.maximum;