create table T1(T1_ID INTEGER);
create table T2(T2_ID INTEGER);

insert into T1 values (1);
insert into T1 values (2);
insert into T1 values (3);

insert into T2 values (2);
insert into T2 values (3);
insert into T2 values (4);

commit;

select * from T1;
select * from T2;

select * from T1 cross join T2;

select * from T1 inner join T2
                  on (T1.T1_ID = T2.T2_ID);

select * from T1 left outer join T2
                  on (T1.T1_ID = T2.T2_ID);

select * from T2 left outer join T1
                  on (T1.T1_ID = T2.T2_ID);

select * from T1 right outer join T2
                  on (T1.T1_ID = T2.T2_ID);

select * from T1 full outer join T2
                  on (T1.T1_ID = T2.T2_ID);

select * from T1 full outer join T2
                  on (T1.T1_ID = T2.T2_ID)
  where T1.T1_ID IS NULL or T2.T2_ID IS NULL;

select T1_ID ID from T1
union
select T2_ID from T2;

select T1_ID ID from T1
union all
select T2_ID from T2;

select T1_ID, 'T1' TABLE_NAME from T1
union all
select T2_ID, 'T2' from T2;

select T1_ID, NULL from T1
union all
select NULL, T2_ID from T2;

select * from T1
minus
select * from T2;

select * from T1
intersect
select * from T2;

select * from T1 where T1_ID in (select T2_ID from T2);

select * from T1 where exists (
      select T2_ID from T2 where T1.T1_ID = T2.T2_ID
);

select T1.T1_ID from T1 inner join T2 on (T1_ID = T2.T2_ID)

select * from (
              select * from (
                            select * from T2
                            ) T22) T222;