-- 1. Data definition language (DDL)
Create table PERSON(
  PERSON_ID INTEGER,
  PERSON_NAME VARCHAR2(200),
  BIRTH_DATE DATE
);
alter table PERSON add primary key(PERSON_ID);

alter table PERSON modify PERSON_NAME VARCHAR2(100);

-- INTEGER
-- NUMBER(38, 2)
-- FLOAT
-- CHAR(10) -- 'Y', 'N', 'RU', 'US'
-- VARCHAR2(4000 CHAR)
-- CLOB (character large object)
-- BLOB (binary large object)
-- DATE
-- TIMESTAMP
-- 'BOOLEAN' - NUMBER(1), CHAR(1)

-- 2. Data manupulation language (DML)
-- 1. INSERT
INSERT INTO PERSON(
    PERSON_ID,
    PERSON_NAME,
    BIRTH_DATE
) values
    (4, 'John Smith', TO_DATE('19.01.1995', 'DD.MM.YYYY'));

-- 2. DELETE
DELETE FROM PERSON where PERSON_ID = 2;

DELETE FROM PERSON;
TRUNCATE TABLE PERSON;

-- 3. UPDATE
UPDATE PERSON set PERSON_NAME = 'Steve Brown',
                  BIRTH_DATE = TO_DATE('20.02.1995', 'DD.MM.YYYY')
where PERSON_ID = 3;

commit;
rollback;

-- 4. SELECT
select * from PERSON;

select PERSON_NAME, PERSON_ID
from PERSON
-- where PERSON_ID > 1 and
--      UPPER(PERSON_NAME) like '%W%'
order by UPPER(PERSON_NAME) desc, PERSON_ID desc
;

select count(distinct PERSON_NAME) from PERSON;

select PRODUCT_ID,
       avg(PRODUCT_QTY),
       max(PRODUCT_QTY),
       min(PRODUCT_QTY),
       count(PRODUCT_QTY),
       sum(PRODUCT_QTY)
from SALES_ORDER_LINE
where PRODUCT_QTY > 5
group by PRODUCT_ID
having avg(PRODUCT_QTY) > 25
order by PRODUCT_ID


select v$instance;