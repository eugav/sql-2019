select count(*) from all_objects;

create table ACCOUNT(ACCOUNT_ID INTEGER, AMOUNT NUMBER(38,2));
alter table ACCOUNT add primary key (ACCOUNT_ID);

delete from ACCOUNT;
insert into ACCOUNT(ACCOUNT_ID, AMOUNT) values (1, 100.0);
insert into ACCOUNT(ACCOUNT_ID, AMOUNT) values (2, 200.0);

commit;

update ACCOUNT set AMOUNT = AMOUNT - 50 where ACCOUNT_ID = 1;
update ACCOUNT set AMOUNT = AMOUNT + 50 where ACCOUNT_ID = 2;

select * from ACCOUNT where AMOUNT < 0 and ACCOUNT_ID in (1,2);

select * from ACCOUNT;

rollback;
commit;

Atomicity
Consistency
Isolation
Durability

1. DIRTY READ
2. READ COMMITTED
3. REPEATABLE READ
4. SERIALIZABLE
