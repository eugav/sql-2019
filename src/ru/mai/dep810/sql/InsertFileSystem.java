package ru.mai.dep810.sql;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class InsertFileSystem {

    public static void main(String[] args) throws IOException {

        final AtomicLong sequence = new AtomicLong(0);
        final Map<Path, Long> assignedIds = new HashMap<>();

        Files.walkFileTree(Paths.get("c:/Program Files"), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                        long id = sequence.getAndIncrement();
                        assignedIds.put(dir, id);
                        insert(dir, id, assignedIds.get(dir.getParent()), "DIR");
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                        if (exc instanceof AccessDeniedException) {
                            return FileVisitResult.SKIP_SUBTREE;
                        }
                        throw exc;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        long id = sequence.getAndIncrement();
                        insert(file, id, assignedIds.get(file.getParent()), "FILE");
                        return FileVisitResult.CONTINUE;
                    }
                }
        );
    }

    private static void insert(Path dir, long id, Long parentId, String type) throws IOException {
        System.out.format(
                "insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE, FILE_SIZE) values (%d, %d, '%s', '%s', %d);\n",
                id,
                parentId,
                escape(dir.getFileName().toString()),
                type,
                Files.size(dir)
        );
    }

    private static Object escape(String fileName) {
        return fileName.replace("'", "''");
    }
}
