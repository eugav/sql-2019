insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (0, NULL, '/', 'DIR');

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (1, 0, 'home', 'DIR');

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (2, 0, 'var', 'DIR');

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (3, 0, 'root', 'DIR');

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (4, 2, 'log', 'DIR');

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (5, 4, 'apache.log', 'FILE');

commit;

insert into FILE_SYSTEM(ID, PARENT_ID, NAME, TYPE)
values (6, 44, 'apache2.log', 'FILE');

select * from FILE_SYSTEM where ID = 2
union all
select * from FILE_SYSTEM where PARENT_ID = 2
union all
select * from FILE_SYSTEM where PARENT_ID in (
          select ID from FILE_SYSTEM where PARENT_ID = 2
    )
union all
select * from FILE_SYSTEM where PARENT_ID in (
       select ID from FILE_SYSTEM where PARENT_ID in (
           select ID from FILE_SYSTEM where PARENT_ID = 2
       )
 );

select ID,
       NAME,
       LEVEL,
       CONNECT_BY_ROOT(NAME),
       CONNECT_BY_ISLEAF,
       CONNECT_BY_ISCYCLE,
       SYS_CONNECT_BY_PATH(name, '/'),
       FILE_SIZE,
       TYPE
               from FILE_SYSTEM
               connect by nocycle prior id = PARENT_ID
               start with ID = 0
order siblings by file_size;

select * from FILE_SYSTEM;
insert into FILE_SYSTEM values (100000, 'd1', 0, 'DIR', 0);
insert into FILE_SYSTEM values (100001, 'd2', 100000, 'DIR', 0);
update FILE_SYSTEM set PARENT_ID = 100001 where ID = 100000;

select * from FILE_SYSTEM where ID in (100000, 100001);

with fs_tree(ID, PARENT_ID, NAME, TYPE, FILE_SIZE) as (
      select ID, PARENT_ID, NAME, TYPE, FILE_SIZE from FILE_SYSTEM where PARENT_ID IS NULL
    union all
     select fs1.ID, fs1.PARENT_ID, fs1.NAME, fs1.TYPE, fs1.FILE_SIZE from FILE_SYSTEM fs1
     inner join fs_tree fs2 on (fs1.PARENT_ID = fs2.ID)
    )
select * from fs_tree;

select count(*) from FILE_SYSTEM;

select 'a', dbms_random.value from dual
    connect by level <= 100;