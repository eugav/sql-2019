drop table FILE_SYSTEM;

create table FILE_SYSTEM(
  ID INTEGER,
  PARENT_ID INTEGER,
  NAME VARCHAR2(2000),
  TYPE VARCHAR2(10),
  FILE_SIZE INTEGER
--  PATH VARCHAR2(4000) /* 1/4/88/1239/3434 */
);

select * from FILE_SYSTEM;

alter table FILE_SYSTEM add primary key (ID);
alter table FILE_SYSTEM add constraint FK_FILE_SYSTEM_PARENT_ID foreign key (PARENT_ID) references FILE_SYSTEM(ID);
create index I_FILE_SYSTEM_PARENT_ID on FILE_SYSTEM(PARENT_ID);

select * from FILE_SYSTEM where PARENT_ID IS NULL
    union all
select * from FILE_SYSTEM where PARENT_ID in (select ID from FILE_SYSTEM where PARENT_ID IS NULL)
    union all
select * from FILE_SYSTEM where PARENT_ID in (
    select ID from FILE_SYSTEM where PARENT_ID in (select ID from FILE_SYSTEM where PARENT_ID IS NULL)
);


select ID, PARENT_ID, LPAD(' ', LEVEL*4) || NAME, SYS_CONNECT_BY_PATH(NAME, '/') from FILE_SYSTEM S
    connect by PARENT_ID = prior ID
    start with PARENT_ID IS NULL;

select ID, PARENT_ID, LPAD(' ', LEVEL*4) || NAME, SYS_CONNECT_BY_PATH(NAME, '/') from FILE_SYSTEM S
connect by ID = prior PARENT_ID
start with ID = 15;

select count(*) from FILE_SYSTEM;

select * from FILE_SYSTEM;

select S.*, LPAD(' ', LEVEL*4) || NAME,
        SYS_CONNECT_BY_PATH(NAME, '/'),
       CONNECT_BY_ISCYCLE,
       CONNECT_BY_ISLEAF,
       CONNECT_BY_ROOT NAME
from FILE_SYSTEM S
    connect by nocycle PARENT_ID = prior ID
    start with ID = 3
    ORDER SIBLINGS BY NAME;

rollback;

update FILE_SYSTEM set PARENT_ID = 8 where ID = 3;

select S.*, LPAD(' ', LEVEL*4) || NAME, CONNECT_BY_ISCYCLE, CONNECT_BY_ISLEAF from FILE_SYSTEM S
connect by nocycle PARENT_ID = prior ID
start with ID = 3;

rollback;

select S.*, LPAD(' ', LEVEL*4) || NAME, SYS_CONNECT_BY_PATH(NAME, '/') from FILE_SYSTEM S
connect by prior PARENT_ID = ID
start with ID = 9;

with fs_tree(ID, PARENT_ID, NAME, TYPE, FILE_SIZE, L) as (
    select ID, PARENT_ID, NAME, TYPE, FILE_SIZE, 1 L from FILE_SYSTEM where PARENT_ID IS NULL
    union all
    select fs1.ID, fs1.PARENT_ID, fs1.NAME, fs1.TYPE, fs1.FILE_SIZE, fs2.L + 1 from FILE_SYSTEM fs1
                                            inner join fs_tree fs2 on (fs1.PARENT_ID = fs2.ID)
)
select * from fs_tree;

select LEVEL, ID, LPAD(' ', LEVEL*4) || NAME, FILE_SIZE, TYPE, PARENT_ID from FILE_SYSTEM
    connect by prior id = PARENT_ID
    start with PARENT_ID IS NULL;

select ID, NAME, TYPE, FILE_SIZE, PARENT_ID from FILE_SYSTEM
connect by id = prior PARENT_ID
start with ID = 11;

with fs_tree(ID, NAME, TYPE, FILE_SIZE, PARENT_ID) as (
    select ID, NAME, TYPE, FILE_SIZE, PARENT_ID from FILE_SYSTEM where PARENT_ID IS NULL
    union all
    select fs1.ID, fs1.NAME, fs1.TYPE, fs1.FILE_SIZE, fs1.PARENT_ID from FILE_SYSTEM fs1
                    inner join fs_tree fs2 on (fs1.PARENT_ID = fs2.ID)
)
select * from fs_tree;

select dbms_random.RANDOM() from dual connect by level <= 12;

update FILE_SYSTEM set PARENT_ID = 8 where ID = 3;

select ID, NAME, TYPE, FILE_SIZE, PARENT_ID, CONNECT_BY_ISCYCLE, CONNECT_BY_ISLEAF from FILE_SYSTEM
connect by nocycle id = prior PARENT_ID
start with ID = 8;

rollback;

select CONNECT_BY_ROOT NAME, ID, NAME, TYPE, FILE_SIZE, PARENT_ID, CONNECT_BY_ISCYCLE, CONNECT_BY_ISLEAF, SYS_CONNECT_BY_PATH(NAME, '/') from FILE_SYSTEM
connect by nocycle prior id = PARENT_ID
start with PARENT_ID IS NULL
order siblings by NAME desc;

create table NODE(ID INTEGER, NAME VARCHAR2(100));

create table NODE_LINK(ID INTEGER, NODE_FROM_ID INTEGER, NODE_TO_ID INTEGER, WEIGTH NUMBER(10));

select NODE_TO_ID, LEVEL from NODE_LINK
connect by nocycle PRIOR NODE_TO_ID = NODE_FROM_ID
start with NODE_FROM_ID = 10;

with fs_tree(ID, PARENT_ID, NAME, TYPE, FILE_SIZE, LVL, PATH) as (
    select ID, PARENT_ID, NAME, TYPE, FILE_SIZE, 1 LVL, '/' || NAME as PATH from FILE_SYSTEM where PARENT_ID IS NULL
    union all
    select fs1.ID, fs1.PARENT_ID, fs1.NAME, fs1.TYPE, fs1.FILE_SIZE, LVL + 1, fs2.PATH || '/' || fs1.NAME
        from FILE_SYSTEM fs1
            inner join fs_tree fs2 on (fs1.PARENT_ID = fs2.ID)
)
select * from fs_tree;

select LEVEL from dual connect by level <= 100;


select * from PRODUCT P inner join PRODUCT_ATTRIBUTE PA on (PA.product_id = P.product_id)
where PA.ATTRIBUTE_ID = 42 and PA.NUM_VALUE > 21  and PA.NUM_VALUE < 27;

select count(*) from FILE_SYSTEM;

with SUMMONTH AS (
    select MANAGER_ID,
           MANAGER_FIRST_NAME,
           MANAGER_LAST_NAME,
           SALE_AMOUNT,
           SUM(SALE_AMOUNT) OVER (PARTITION BY MANAGER_ID
               order by SALE_DATE range between interval '3' month preceding and current row) SUMDAY,
           TO_CHAR(SALE_DATE,'YYYY-MM') SALE_MONTH
    from V_FACT_SALE WHERE (SALE_DATE <= TO_DATE('2014-12-31','YYYY-MM-DD') AND SALE_DATE >= TO_DATE('2013-11-01','YYYY-MM-DD'))
)
with SUMMONTH AS (
    select MANAGER_ID,
           MANAGER_FIRST_NAME,
           MANAGER_LAST_NAME,
           SALE_AMOUNT,
           SUM(SALE_AMOUNT) OVER (PARTITION BY MANAGER_ID
               order by SALE_DATE range between interval '3' month preceding and current row) SUMDAY,
           TO_CHAR(SALE_DATE,'YYYY-MM') SALE_MONTH
    from V_FACT_SALE WHERE (SALE_DATE <= TO_DATE('2014-12-31','YYYY-MM-DD') AND SALE_DATE >= TO_DATE('2013-11-01','YYYY-MM-DD'))
)

WITH MAN_THREE_MONTH_SUM AS
         (SELECT MANAGER_ID,
                 SALE_DATE,
                 MANAGER_FIRST_NAME,
                 MANAGER_LAST_NAME,
                 SALE_AMOUNT,
                 SUM(SALE_AMOUNT) OVER (
                     PARTITION BY MANAGER_ID
                     ORDER BY SALE_DATE RANGE BETWEEN INTERVAL '3' MONTH PRECEDING AND
                     INTERVAL '1' MONTH PRECEDING
                     ) AS SUM_BY_THREE_MONTHS
          FROM V_FACT_SALE
          WHERE SALE_DATE BETWEEN TO_DATE('2013-10-01', 'YYYY-MM-DD') AND TO_DATE('2014-12-31', 'YYYY-MM-DD')
         )
SELECT
    DISTINCT MANAGER_ID,
             MANAGER_FIRST_NAME,
             MANAGER_LAST_NAME,
             TO_CHAR(SALE_DATE, 'MM') AS MAN_MONTH,
             (SUM_BY_THREE_MONTHS*0.05) AS BONUS,
             SUM_BY_THREE_MONTHS
FROM MAN_THREE_MONTH_SUM
WHERE SUM_BY_THREE_MONTHS IN (
    SELECT MAX(SUM_BY_THREE_MONTHS) OVER (
        PARTITION BY TO_CHAR(SALE_DATE, 'MM')
        ) AS MAX_BY_MONTH
    FROM MAN_THREE_MONTH_SUM
    WHERE SALE_DATE BETWEEN TO_DATE('2014-01-01', 'YYYY-MM-DD') AND TO_DATE('2014-12-31', 'YYYY-MM-DD')
)
ORDER BY MAN_MONTH;

with v_fact_sale_by_date as (
    select MANAGER_ID,
           MANAGER_FIRST_NAME,
           MANAGER_LAST_NAME,
           to_date(to_char(SALE_DATE,'mm-yyyy'),'mm-yyyy') as month_year,
           --extract(year from SALE_DATE) as year,
           sum(SALE_AMOUNT) as sum_per_month
    from V_FACT_SALE
    group by  MANAGER_ID,  MANAGER_FIRST_NAME, MANAGER_LAST_NAME,
              to_date(to_char(SALE_DATE,'mm-yyyy'),'mm-yyyy')
)
select extract(month from month_year) as month_number
     ,manager_id
     ,MANAGER_FIRST_NAME
     ,MANAGER_LAST_NAME
     ,backsum as premia
from (
         select v_fact_sale_by_date.*
              ,0.05 * sum(sum_per_month) over (partition by manager_id
             order by month_year
             range between interval '3' month preceding
             and interval  '0' month preceding ) backsum
              ,max(sum_per_month) over (partition by month_year) max_sum_month
         from v_fact_sale_by_date
     ) temp
where sum_per_month = max_sum_month
  and extract(year from month_year) = 2014;

WITH MANAGER_SOLD AS
         (
             SELECT DISTINCT
                 EXTRACT(MONTH FROM SALE_DATE) SALES_MONTH,
                 MANAGER_ID,
                 SUM(SALE_PRICE) OVER(PARTITION BY MANAGER_ID ORDER BY SALE_DATE RANGE BETWEEN INTERVAL '3' MONTH PRECEDING AND CURRENT ROW) SALES_AMOUNT
             FROM V_FACT_SALE vfs
             WHERE EXTRACT(YEAR FROM SALE_DATE) = 2014
         ),
     MANAGER_NAMES AS(
         SELECT DISTINCT MANAGER_ID,
                         MANAGER_FIRST_NAME,
                         MANAGER_LAST_NAME
         FROM V_FACT_SALE
     ),
     BONUS AS (
         SELECT SALES_MONTH,
                SUM(SALE_PRICE)*0.05 AS BONUS
         FROM (
                  SELECT
                      EXTRACT(MONTH FROM SALE_DATE) SALES_MONTH,
                      SALE_PRICE
                  FROM V_FACT_SALE
              )
         GROUP BY SALES_MONTH
     )
SELECT MSLD.SALES_MONTH,
       MSLD.MANAGER_ID,
       MANAGER_NAMES.MANAGER_FIRST_NAME,
       MANAGER_NAMES.MANAGER_LAST_NAME,
       BONUS.BONUS
FROM MANAGER_SOLD MSLD
         JOIN (SELECT SALES_MONTH, MAX(SALES_AMOUNT) AS MAX_SOLD
               FROM MANAGER_SOLD
               GROUP BY SALES_MONTH) SLD
              ON MSLD.SALES_MONTH = SLD.SALES_MONTH AND MSLD.SALES_AMOUNT = SLD.MAX_SOLD
         JOIN MANAGER_NAMES
              ON MSLD.MANAGER_ID = MANAGER_NAMES.MANAGER_ID
         JOIN BONUS
              ON MSLD.SALES_MONTH = BONUS.SALES_MONTH
ORDER BY MSLD.SALES_MONTH;



WITH temp AS (
    SELECT manager_id, manager_first_name, manager_last_name, sale_date,
           SUM(sale_amount) OVER(PARTITION BY manager_id ORDER BY sale_date
               RANGE BETWEEN INTERVAL '3' month PRECEDING AND CURRENT ROW) as three_month_amount,
           EXTRACT(MONTH FROM sale_date) sale_month,
           EXTRACT(YEAR FROM sale_date) sale_year
    FROM v_fact_sale
)
SELECT sale_month, manager_id, manager_first_name, manager_last_name,
       (three_month_amount * 0.05) prize
FROM temp
WHERE three_month_amount =
      (SELECT MAX(three_month_amount) FROM temp t WHERE t.sale_month = temp.sale_month AND t.sale_year = temp.sale_year)
  AND sale_date BETWEEN TO_DATE('01.01.2014', 'DD.MM.YYYY') AND TO_DATE('31.12.2014', 'DD.MM.YYYY')
GROUP BY manager_id, manager_first_name, manager_last_name, three_month_amount, sale_month
ORDER BY sale_month;

with rec(id, parent_id, name, type, file_size, L, path) as (
    select id, parent_id, name, type, file_size,
           1 L, '/' || name path
    from file_system
    where parent_id is NULL
    union all
    select fs1.id, fs1.parent_id, fs1.name, fs1.type, fs1.file_size
         ,fs2.L + 1
         , fs2.path || '/' || fs1.name path
    from file_system fs1 inner join rec fs2 on fs1.parent_id = fs2.id
)
,
-- select * from rec;
     summator(id, parent_id, name, type, file_size, L, path, sum_size) as (
         select id
              , parent_id
              , name
              , type
              , file_size
              , L
              , path
              , file_size as sum_size
         from rec
              --where L = (select max(L) from rec)
         where type = 'FILE'

         union all

         select fs1.id
              , fs1.parent_id
              , fs1.name
              , fs1.type
              , fs1.file_size
              , fs1.L
              , fs1.path
              , fs2.sum_size +
         --(select sum(file_size) from rec where rec.parent_id = fs1.id group by rec.parent_id) sum_size
         --, fs1.file_size + --fs2.file_size sum_size
         --sum(fs2.sum_size)
         --sum(fs2.file_size)
         --sum (fs2.sum_size) over (partition by fs2.parent_id) sum_size
                (select sum(sum_size)
                 from summator
                 where parent_id = fs1.parent_id) as sum_size
         from rec fs1
                  join summator fs2
                       on fs2.parent_id = fs1.id
         --group by fs1.id, fs1.parent_id, fs1.name, fs1.type, fs1.file_size, fs1.L, fs1.path
         --WHERE fs.TYPE = 'DIR'
     )
select * from summator
where type = 'DIR';